/*******************************************************************************
 *
 *                  Libreria PCD8544
 *
 *******************************************************************************
 * FileName:        Nokia5110.h
 * Processor:       PICxxxxxx
 * Complier:        XC8 v1.37
 * Author:          Brandon Ruiz Vasquez
 * Email:           brandon.ruiz@udem.edu
 * Description:     Libreria creado para una LCD Nokia 5110, la pantalla requiere
 *                  de una comunicacion SPI pero se ha implementado una funcion
 *                  que realiza dicha tarea asique de puede usar cualquier pin
 *                  del uC.
 ******************************************************************************/
#ifndef Nokia5110_H
#define	Nokia5110_H

/**********P O R T * L C D * C O N F I G***************************************/
#define TRIS_pinRST     TRISB0
#define pinRST          PORTBbits.RB0 //Reset
#define TRIS_pinCE      TRISB1
#define pinCE           PORTBbits.RB1 //Chip Enable
#define TRIS_pinDC      TRISB2
#define pinDC           PORTBbits.RB2 //Data = 1 or Command = 0
#define TRIS_pinSDOUT   TRISB3
#define pinSDOUT        PORTBbits.RB3 //Data serial ouput(in LCD = DIN)
#define TRIS_pinSCLK    TRISB4
#define pinSCLK         PORTBbits.RB4 //Clock

/**********L C D * D E F I N E S***********************************************/
//The DC pin tells to LCD if we are sending a command or data
#define LCD_COMMAND 0
#define LCD_DATA    1
//You may find a different size screen, but this one is 84 by 48 pixels
#define LCD_X       84
#define LCD_Y       48

/**********G E N E R A L * D E F I N E S***************************************/
#define Nokia5110_HIGH    1
#define Nokia5110_LOW     0
#define Nokia5110_OUTPUT  0

/**********P R O T O T Y P E S*************************************************/
void Nokia5110_Init(void);
void Nokia5110_Clear(void);
void Nokia5110_GotoXY(unsigned char X, unsigned char Y);
void Nokia5110_WriteByte(char DATA_CHAR);
void Nokia5110_WriteString(char *DATA_STRING, unsigned char X, unsigned char Y);
void Nokia5110_Bitmap(char MY_ARRAY[], unsigned char X, unsigned char Y);
void Nokia5110_Write(unsigned char MODE,unsigned char DATA);
void Nokia5110_WriteSPI(char outputDATA);

#endif	/* Nokia5110_H */

