/*
 * File:   main_PinChangeInt.c
 * Author: Brandy
 *
 * Created on 16 de noviembre de 2016, 19:47
 */


#include <xc.h>
#include "Alteri.h"
//#include "Nokia5110.h"

unsigned char config1;
unsigned int timer_value;
unsigned int tpr;
int counter=0;

void main(void) {
    TRISDbits.RD0 = 0;
    TRISBbits.RB0 = 1;
    ADCON0=0x00; //all digital
    ADCON1bits.PCFG = 0b1111;   //All Pins Digital
    INTCONbits.GIE = 1;         // Enable Global Interrupt
    INTCONbits.RBIE = 1;
    //RCONbits.IPEN = 0;
    //INTCONbits.PEIE = 1;      // Enable Perpherial Interrupt
    INTCON2bits.RBPU = 0;   //PORTB pull-ups are enabled by individual port latch values
    INTCON2bits.INTEDG0 = 1;
    INTCON2bits.RBIP = 0;
    //INTCON2bits.NOT_RBPU = 0;
    //TMR1 = 0;
    while(1)
    {
        delay_us(1);
    }
}

void interrupt low_priority ISR(void) {
// Was it the port B interrupt on change?
    if (INTCONbits.RBIF) {
        // Dummy read of the port, as per datasheet
        // Use XOR to toggle the pin, saving a variable
        LATDbits.LATD0 ^= 1;
        // Reset the interrupt flag
        INTCONbits.RBIF = 0;
        delay_us(1);
    }
}